Ejercicio Tecnico de Mercado Libre hecho con Android Kotlin y MvvM

Activies
 - Splash Activity: Primera actividad que se presenta, en ella se carga la lista de categorias en un Room consultando con un Get a la Api a traves de una Coroutine
 - Main Activity: Actividad principal con un boton para realizar una busqueda, tabs con todas las categorias previamente cargadas en el Room y un listado de items de la categoria seleccionada
 - Search Activity: Actividad con un resultado de la busqueda hecha
 - Detail activity: Actividad con un detalle del producto seleccionado. Se puede accedes a esta actividad clickeando en un item ya sea en la actividad principal o en el listado de busqueda
 - Searchable Activity: Actividad necesaria para realizar la busqueda
 - Placeholde fragment: Fragment utilizado para mostrar un listado de items segun la categoria seleccionada


Eh optado por seguir una arquitectura Mvvm para diseñar la app. Bajo esta arquitectura se para cada Activity en 3 package
- UI: Aqui se encuentran todas las activities correspondiente a cada vista y en ellas solo se manejan cosas de la ui como ocultar el loading
- View Model: Aqui se manejara la data que traigo de las api proporcionada por ustedes. Utilize Live Data para manejar dichos datos
- Repository: Manejo de las coroutines donde ser utilizaran las apis
- Models: modelos necesarios para acomodar la data

Adicionalmente al manejo de las api, para las categorias utilice Room para de esta manera almacenarlo una vez traidas en el splash y no tener que estar consultando todo el tiempo

