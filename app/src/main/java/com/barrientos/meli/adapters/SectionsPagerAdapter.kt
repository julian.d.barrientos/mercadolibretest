package com.barrientos.meli.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.barrientos.meli.models.Category
import com.barrientos.meli.ui.PlaceholderFragment

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(fm: FragmentManager, private var _categories: Array<Category> = arrayOf()) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return PlaceholderFragment.newInstance(_categories[position].id)
    }

    override fun getPageTitle(position: Int): CharSequence? {

        return _categories[position].name
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return _categories.size
    }
    fun setCategories(categories: Array<Category> ){
        _categories = categories
        notifyDataSetChanged()
    }
}