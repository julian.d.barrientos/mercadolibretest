package com.barrientos.meli.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.barrientos.meli.R
import com.barrientos.meli.models.Pictures
import com.bumptech.glide.Glide

class ItemGalleryAdapter(private var images: List<Pictures> = listOf()): RecyclerView.Adapter<ItemGalleryAdapter.GalleryHolder>() {

    inner class GalleryHolder(val view: View) : RecyclerView.ViewHolder(view){

        fun bindItems(item: Pictures){
            val circularProgressDrawable = CircularProgressDrawable(view.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            val image : ImageView = itemView.findViewById(R.id.gallery_image)
            Glide.with(itemView)
                .load(item.url)
                .placeholder(circularProgressDrawable)
                .into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_gallery,parent,false)
        return GalleryHolder(v)
    }

    override fun getItemCount() = this.images.size

    override fun onBindViewHolder(holder: GalleryHolder, position: Int) {
        val item = images[position]
        holder.bindItems(item)
    }

    fun setData(newPictures: List<Pictures>){
        this.images = newPictures
        notifyDataSetChanged()
    }
}