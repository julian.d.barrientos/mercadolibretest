package com.barrientos.meli.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.barrientos.meli.ui.DetailActivity
import com.barrientos.meli.models.Item
import com.barrientos.meli.R
import com.bumptech.glide.Glide
import java.text.NumberFormat
import java.util.*

class ItemsAdapter(private var items: List<Item>) :
    RecyclerView.Adapter<ItemsAdapter.ItemsHolder>() {

    var format: NumberFormat = NumberFormat.getCurrencyInstance(Locale("es","AR"))
    inner class ItemsHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bindItems(item: Item){
            val circularProgressDrawable = CircularProgressDrawable(view.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()

            val itemTitle : TextView = itemView.findViewById(R.id.card_item_title)
            val image : ImageView = itemView.findViewById(R.id.card_item_image)
            val currency: TextView = itemView.findViewById(R.id.card_item_price)
            val currencyDecimal: TextView = itemView.findViewById(R.id.card_item_decimal)
            val formatted = format.format(item.price).split(",")
            itemTitle.text = item.title
            currency.text = formatted[0]
            currencyDecimal.text = formatted[1]
            Glide.with(itemView)
                .load(item.thumbnail)
                .placeholder(circularProgressDrawable)
                .into(image)
            itemView.setOnClickListener{
                val contxt: Context = it.context
                val intet = Intent(contxt, DetailActivity::class.java)
                intet.putExtra("itemId",item.id)
                contxt.startActivity(intet)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ItemsHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
        return ItemsHolder(v)
    }
    override fun onBindViewHolder(holder: ItemsHolder, position: Int) {
        val item = items[position]
        holder.bindItems(item)
    }
    override fun getItemCount() = items.size

    fun setData(newItems: List<Item>){
        this.items = newItems
        notifyDataSetChanged()
    }
}