package com.barrientos.meli.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.R
import com.barrientos.meli.models.Attribute

class ItemAttributesAdapter (private var attributes: List<Attribute> = listOf()):RecyclerView.Adapter<ItemAttributesAdapter.AttributesHolder> (){

    private val limit = 7

    inner class AttributesHolder(val view: View) : RecyclerView.ViewHolder(view){

        fun bindItems(item: Attribute, position: Int){
            val name = itemView.findViewById<TextView>(R.id.att_name)
            val value = itemView.findViewById<TextView>(R.id.att_value)

            name.text = item.name
            value.text = item.valueName

            if(position % 2 == 0) name.setBackgroundColor(itemView.context.getColor(R.color.light_gray_inactive_icon))
            else name.setBackgroundColor(itemView.context.getColor(R.color.light_gray))

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttributesHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_attribute,parent,false)
        return AttributesHolder(v)
    }

    override fun getItemCount() = if(this.attributes.size >limit) limit else attributes.size

    override fun onBindViewHolder(holder: AttributesHolder, position: Int) {
        val item = attributes[position]
        holder.bindItems(item,position)
    }

    fun setData(newPictures: List<Attribute>){
        this.attributes = newPictures
        notifyDataSetChanged()
    }
}