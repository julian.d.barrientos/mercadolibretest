package com.barrientos.meli.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.barrientos.meli.api.RetrofitBuilder
import com.barrientos.meli.models.Category
import kotlinx.coroutines.*

object SplashRepository {
    var job: CompletableJob? = null

    fun getCategories(): MutableLiveData<Array<Category>> {
        job = Job()
        return object : MutableLiveData<Array<Category>>(){
            override fun onActive() {
                super.onActive()
                job?.let { actualJob ->
                    CoroutineScope(Dispatchers.IO + actualJob).launch {
                        val item =  RetrofitBuilder.apiService.getCategories()
                        withContext(Dispatchers.Main){
                            value = item

                            actualJob.complete()
                        }
                    }
                }
            }
        }
    }

    fun cancelJobs(){
        job?.cancel()
    }
}