package com.barrientos.meli.repositories

import androidx.lifecycle.LiveData
import com.barrientos.meli.api.RetrofitBuilder
import com.barrientos.meli.models.Item
import com.barrientos.meli.models.ItemDetail
import kotlinx.coroutines.*

object SearchRepository {

    var job: CompletableJob? = null

    fun getSearchItems(_itemId:String): LiveData<List<Item>> {
        job = Job()
        return object : LiveData<List<Item>>(){
            override fun onActive() {
                super.onActive()
                job?.let { actualJob ->
                    CoroutineScope(Dispatchers.IO + actualJob).launch {
                        val item =  RetrofitBuilder.apiService.getSearchItems(_itemId).results
                        withContext(Dispatchers.Main){
                            value = item
                            actualJob.complete()
                        }
                    }
                }
            }
        }
    }

    fun cancelJobs(){
        job?.cancel()
    }
}