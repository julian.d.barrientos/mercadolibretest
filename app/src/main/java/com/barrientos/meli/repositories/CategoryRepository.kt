package com.barrientos.meli.repositories

import android.app.Application
import com.barrientos.meli.models.Category
import com.barrientos.meli.room.dao.CategoryDao
import com.barrientos.meli.room.database.CategoryDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import kotlin.coroutines.CoroutineContext

class CategoryRepository(application: Application): CoroutineScope{
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var _categoryDao:CategoryDao

    init {
        val db = CategoryDatabase.getDataBase(application)
        _categoryDao = db.categoryDao()
    }

    fun getCategories() = _categoryDao.getCategories()

    fun setCategory (category: Category){
        launch { setCategoryBG(category) }
    }
    fun setCategories (categories: Array<Category>){
        launch { setCategoriesBG(categories) }
    }

    private suspend fun setCategoryBG(category: Category){
        withContext(Dispatchers.IO){
            _categoryDao.setCategory(category)
        }
    }

    private suspend fun setCategoriesBG(categories: Array<Category>){
        withContext(Dispatchers.IO){
            _categoryDao.setCategories(categories)
        }
    }
}