package com.barrientos.meli.repositories


import androidx.lifecycle.LiveData
import com.barrientos.meli.api.RetrofitBuilder
import com.barrientos.meli.models.Catalog
import com.barrientos.meli.models.ItemDetail
import com.barrientos.meli.models.Vendor
import com.google.gson.Gson
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main


object DetailRepository {

    var job: CompletableJob? = null

    fun getDetailItem(_itemId:String):LiveData<ItemDetail>{
        job = Job()
        return object : LiveData<ItemDetail>(){
            override fun onActive() {
                super.onActive()
                job?.let { actualJob ->
                    CoroutineScope(IO + actualJob).launch {
                        val item =  RetrofitBuilder.apiService.getItemDetail(_itemId)
                        withContext(Main){
                            value = item
                            actualJob.complete()
                        }
                    }
                }
            }
        }
    }

    fun getVendor(_vendorId: String):LiveData<Vendor>{
        job = Job()
        return object : LiveData<Vendor>(){
            override fun onActive() {
                super.onActive()
                job?.let { actualJob ->
                    CoroutineScope(IO + actualJob).launch {
                        val vendor = RetrofitBuilder.apiService.getVendorDetail(_vendorId)
                        withContext(Main){
                            value = vendor
                            actualJob.complete()
                        }
                    }
                }
            }
        }
    }

    fun getAttributes(_catalogId: String):LiveData<Catalog>{
        job = Job()
        return object : LiveData<Catalog>(){
            override fun onActive() {
                super.onActive()
                job?.let { actualJob ->
                    CoroutineScope(IO + actualJob).launch {
                        val catalog = RetrofitBuilder.apiService.getCatalogProducts(_catalogId)
                        withContext(Main){
                            value = catalog
                            actualJob.complete()
                        }
                    }
                }
            }
        }
    }
    fun cancelJobs(){
        job?.cancel()
    }
}