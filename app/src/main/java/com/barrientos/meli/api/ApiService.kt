package com.barrientos.meli.api

import com.barrientos.meli.models.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService{
    @GET("sites/MLA/search")
    suspend fun getItemFromCategory(
        @Query("category") categoryId:String
    ):Response

    @GET("items/{itemId}")
    suspend fun getItemDetail(
        @Path("itemId")itemId:String
    ):ItemDetail

    @GET("sites/MLA/search")
    suspend fun getSearchItems(
        @Query("q") search:String
    ):Response

    @GET("sites/MLA/categories")
    suspend fun getCategories():Array<Category>

    @GET ("users/{vendorId}")
    suspend fun getVendorDetail(
        @Path("vendorId")vendorId:String
    ):Vendor

    @GET("catalog_products/{catalogId}")
    suspend fun getCatalogProducts(
        @Path("catalogId")catalogId:String
    ):Catalog
}