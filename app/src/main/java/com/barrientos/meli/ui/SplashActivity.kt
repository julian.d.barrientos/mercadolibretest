package com.barrientos.meli.ui

import com.barrientos.meli.R
import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.barrientos.meli.models.MapClass
import com.barrientos.meli.viewmodels.CategoriesViewModel
import com.barrientos.meli.viewmodels.SplashViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class SplashActivity: AppCompatActivity() {

    private lateinit var  _viewModel: SplashViewModel
    private lateinit var  _categoriesViewModel: CategoriesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setViewModel()
    }

    private fun setViewModel(){
        _viewModel = ViewModelProviders.of(this)[SplashViewModel::class.java]
        _categoriesViewModel = ViewModelProviders.of(this)[CategoriesViewModel::class.java]
        _viewModel.getCategories().observe(this, Observer {
            _categoriesViewModel.setCategories(it)
            applicationContext.startActivity(Intent(applicationContext, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        })
    }

}