package com.barrientos.meli.ui


import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.barrientos.meli.R
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.adapters.ItemAttributesAdapter
import com.barrientos.meli.adapters.ItemGalleryAdapter
import com.barrientos.meli.databinding.ActivityDetailBinding
import com.barrientos.meli.models.Attribute
import com.barrientos.meli.models.ItemDetail
import com.barrientos.meli.models.Pictures
import com.barrientos.meli.viewmodels.DetailViewModel



class DetailActivity: AppCompatActivity() {

    private lateinit var detailModel: DetailViewModel
    private lateinit var picturesRecyclerView: RecyclerView
    private lateinit var attributesRecyclerView: RecyclerView
    private lateinit var attributesAdapter: ItemAttributesAdapter
    private lateinit var galleryAdapter: ItemGalleryAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var detalle: LinearLayout
    private lateinit var dataBinding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //Set
        dataBinding = DataBindingUtil.setContentView(this,R.layout.activity_detail)
        dataBinding.lifecycleOwner = this
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setViewModel()
        setUI()
        setRecycler()

    }
    private fun setUI(){
        this.picturesRecyclerView = findViewById(R.id.ReView_gallery)
        this.detalle = findViewById(R.id.detalle)
        this.attributesRecyclerView = findViewById(R.id.attributes_list)
    }
    private fun setRecycler(){
        //Set Pictures list with horizontal view
        viewManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        picturesRecyclerView.setHasFixedSize(true)
        picturesRecyclerView.layoutManager = viewManager
        galleryAdapter = ItemGalleryAdapter()
        picturesRecyclerView.adapter = galleryAdapter

        //set list of attributes
        attributesRecyclerView.setHasFixedSize(true)
        attributesRecyclerView.layoutManager = LinearLayoutManager(this)
        attributesRecyclerView.isNestedScrollingEnabled = false
        attributesAdapter = ItemAttributesAdapter()
        attributesRecyclerView.adapter = attributesAdapter
    }

    private fun setViewModel(){
        detailModel = ViewModelProviders.of(this)[DetailViewModel::class.java]
        dataBinding.viewModel = detailModel

        detailModel.itemDetail.observe(this, Observer { item ->
            galleryAdapter.setData(item.pictures)
            //Check for catalog id, if null set attributes with  items
            if(item.catalogProductId == null)  attributesAdapter.setData(item.attributes)
        })
        detailModel.attributes.observe(this, Observer { item ->
            //If catalog id is not null set attributes with server
            if (item.attributes.isNotEmpty() )attributesAdapter.setData(item.attributes)
        })

        //Set item id from extra the first time the activity load to store in view model
        if(intent.getStringExtra("itemId") != null) detailModel.setItenId(intent.getStringExtra("itemId"))
    }


    override fun onDestroy() {
        super.onDestroy()
        detailModel.cancelJobs()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
