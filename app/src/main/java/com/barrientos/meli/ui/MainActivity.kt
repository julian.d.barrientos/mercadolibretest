package com.barrientos.meli.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.barrientos.meli.adapters.SectionsPagerAdapter
import com.barrientos.meli.R
import com.barrientos.meli.providers.SuggestProvider
import com.barrientos.meli.models.Category
import com.barrientos.meli.viewmodels.MainViewModel
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    private lateinit var viewModel: MainViewModel
    private lateinit var tabs: TabLayout
    private lateinit var pagerAdapter: SectionsPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setViewModel()
        setUI()
        handleSearch(intent)
        setSupportActionBar(findViewById(R.id.my_toolbar))
    }



    private fun setUI(){
        //Get Tab view and View Page
        viewPager = findViewById(R.id.view_pager)
        tabs = findViewById(R.id.tabs)

        //Set the tabs and Page Adapter
        pagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter
        tabs.setupWithViewPager(viewPager)
    }
    private fun setViewModel(){
        //SeT Main View model
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getCategories().observe(this, Observer<Array<Category>> {
            pagerAdapter.setCategories(it)
        })
    }
    private fun handleSearch(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                //Add last search to search history
                SearchRecentSuggestions(this, SuggestProvider.AUTHORITY, SuggestProvider.MODE).saveRecentQuery(query, null)

                //Go to activity to search
                startActivity(Intent(this, SearchActivity::class.java).putExtra("search",query))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the options menu from XML
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)

        // Get the SearchView and set the searchable configuration
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            // Assumes current activity is the searchable activity
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(true) // Do not iconify the widget; expand it by default
        }
        return true
    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleSearch(intent)
    }
}
