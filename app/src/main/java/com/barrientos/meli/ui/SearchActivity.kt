package com.barrientos.meli.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.models.Item
import com.barrientos.meli.adapters.ItemsAdapter
import com.barrientos.meli.R
import com.barrientos.meli.providers.SuggestProvider
import com.barrientos.meli.viewmodels.SearchViewModel


class SearchActivity: AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var loading: ProgressBar
    private lateinit var toolbar: Toolbar
    private var items: List<Item> = arrayListOf()
    private lateinit var  viewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setUI()
        setRecyclerView()
        setViewModel()
    }


    private fun setUI(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewManager = LinearLayoutManager(this)
        loading = findViewById(R.id.pg_loading)
        recyclerView = findViewById(R.id.ReView_items)

    }
    private fun setViewModel(){
        viewModel = ViewModelProviders.of(this)[SearchViewModel::class.java]
        viewModel.searchData.observe(this, Observer {
            (viewAdapter as ItemsAdapter).setData(it)
            loading.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        })
        if(intent.getStringExtra("search") != null)viewModel.setSearchId(intent.getStringExtra("search"))
    }
    private fun setRecyclerView(){
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = viewManager
        viewAdapter = ItemsAdapter(items)
        recyclerView.adapter = viewAdapter
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }



}