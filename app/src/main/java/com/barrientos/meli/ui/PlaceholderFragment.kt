package com.barrientos.meli.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arlib.floatingsearchview.FloatingSearchView
import com.barrientos.meli.models.Item
import com.barrientos.meli.R
import com.barrientos.meli.adapters.ItemsAdapter
import com.barrientos.meli.viewmodels.CategoryFragmentViewModel
import com.barrientos.meli.viewmodels.DetailViewModel

/**
 * A placeholder fragment containing a simple view.
 */


class PlaceholderFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: ItemsAdapter
    private lateinit var mSearchView: FloatingSearchView
    private var mLastQuery:String = ""
    private var items: List<Item> = arrayListOf()
    private lateinit var loading: ProgressBar
    private var categoryId: String? = null
    private lateinit var viewModel: CategoryFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryId = arguments?.getString(ARG_SECTION_NUMBER)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        setUi(root)
        setRecycler(root)
        setViewModel()
        return root
    }


    private fun setUi(root: View){
        loading = root.findViewById(R.id.pg_loading)
        recyclerView = root.findViewById(R.id.ReView_items)
    }
    private fun setRecycler(root:View){
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(root.context)
        mAdapter = ItemsAdapter(items)
        recyclerView.adapter = mAdapter
    }
    private fun setViewModel(){
        viewModel = ViewModelProviders.of(this)[CategoryFragmentViewModel::class.java]
        viewModel.itemFromCategory.observe(this, Observer {
            items = it
            loading.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            mAdapter.setData(it)
        })
        viewModel.setCategoryId(categoryId!!)
    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(categoryId: String): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_SECTION_NUMBER, categoryId)
                }
            }
        }
    }
}