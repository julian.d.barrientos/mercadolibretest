package com.barrientos.meli.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.barrientos.meli.models.Item
import com.barrientos.meli.models.ItemDetail
import com.barrientos.meli.repositories.DetailRepository
import com.barrientos.meli.repositories.SearchRepository
import java.text.NumberFormat
import java.util.*

class SearchViewModel: ViewModel() {
    private val _searchId = MutableLiveData<String>()
    val format = NumberFormat.getCurrencyInstance(Locale("es","AR"))

    val searchData: LiveData<List<Item>> = Transformations.switchMap(_searchId){
        SearchRepository.getSearchItems(it)
    }

    fun setSearchId(itemId: String?){
        if(_searchId.value == itemId){
            return
        }
        _searchId.value = itemId
    }

    fun cancelJobs(){
        DetailRepository.cancelJobs()
    }
}