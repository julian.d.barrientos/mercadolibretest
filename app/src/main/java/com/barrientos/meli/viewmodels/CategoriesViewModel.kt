package com.barrientos.meli.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.barrientos.meli.models.Category
import com.barrientos.meli.repositories.CategoryRepository

class CategoriesViewModel(application: Application) : AndroidViewModel(application) {

    private var repository:CategoryRepository = CategoryRepository(application)

    fun getCategories() = repository.getCategories()

    fun setCategory(category: Category) { repository.setCategory(category)}

    fun setCategories(categories: Array<Category>) { repository.setCategories(categories)}

}