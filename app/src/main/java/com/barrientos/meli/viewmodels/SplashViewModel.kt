package com.barrientos.meli.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.barrientos.meli.models.Category
import com.barrientos.meli.models.Item
import com.barrientos.meli.repositories.CategoryRepository
import com.barrientos.meli.repositories.DetailRepository
import com.barrientos.meli.repositories.SearchRepository
import com.barrientos.meli.repositories.SplashRepository

class SplashViewModel : ViewModel(){

    private var _categories = MutableLiveData<Array<Category>>()

    init {
        _categories = SplashRepository.getCategories()
    }

    fun getCategories(): LiveData<Array<Category>> {
        return _categories
    }

}