package com.barrientos.meli.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.barrientos.meli.models.Item
import com.barrientos.meli.repositories.ItemsFromCategoryRepository
import com.barrientos.meli.repositories.DetailRepository
import java.text.NumberFormat
import java.util.*

class CategoryFragmentViewModel : ViewModel(){
    private val _categoryId = MutableLiveData<String>()
    val format = NumberFormat.getCurrencyInstance(Locale("es","AR"))

    val itemFromCategory: LiveData<List<Item>> = Transformations.switchMap(_categoryId){
        ItemsFromCategoryRepository.getItemFromCategory(it)
    }

    fun setCategoryId(categoryId: String){
        if(_categoryId.value == categoryId){
            return
        }
        _categoryId.value = categoryId
    }

    fun cancelJobs(){
        DetailRepository.cancelJobs()
    }
}