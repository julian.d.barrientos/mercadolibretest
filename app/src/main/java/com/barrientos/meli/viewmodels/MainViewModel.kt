package com.barrientos.meli.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.barrientos.meli.adapters.SectionsPagerAdapter
import com.barrientos.meli.models.Category
import com.barrientos.meli.repositories.CategoryRepository


class MainViewModel(application: Application) : AndroidViewModel(application) {

    private var _repository = CategoryRepository(application)

    fun getCategories():LiveData<Array<Category>>{
      return  _repository.getCategories()
    }
}