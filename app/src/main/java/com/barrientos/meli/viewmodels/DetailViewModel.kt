package com.barrientos.meli.viewmodels

import androidx.lifecycle.*
import com.barrientos.meli.models.Attribute
import com.barrientos.meli.models.Catalog
import com.barrientos.meli.models.ItemDetail
import com.barrientos.meli.models.Vendor
import com.barrientos.meli.repositories.DetailRepository
import java.text.NumberFormat
import java.util.*

class DetailViewModel : ViewModel() {

    private val _itemId = MutableLiveData<String>()
    val format = NumberFormat.getCurrencyInstance(Locale("es","AR"))
    var formattedPrice = MutableLiveData<String>()
    var formattedDecimal = MutableLiveData<String>()
    val itemDetail: LiveData<ItemDetail> = Transformations.switchMap(_itemId){
            DetailRepository.getDetailItem(it)
    }
    val vendor: LiveData<Vendor> = Transformations.switchMap(itemDetail){
            val formattedArray = format.format(it.price).split(",")
            formattedPrice.value = formattedArray[0]
            formattedDecimal.value = formattedArray[1]
            DetailRepository.getVendor(it.sellerId)
    }
    val attributes: LiveData<Catalog> = Transformations.switchMap(itemDetail){
        if (it.catalogProductId != null)DetailRepository.getAttributes(it.catalogProductId!!) else MutableLiveData<Catalog>()
    }

    fun setItenId(itemId: String?){
        if(_itemId.value == itemId){
            return
        }
        _itemId.value = itemId
    }

    fun cancelJobs(){
        DetailRepository.cancelJobs()
    }
}