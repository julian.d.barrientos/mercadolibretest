package com.barrientos.meli.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.barrientos.meli.models.Category
import com.barrientos.meli.room.dao.CategoryDao

@Database(entities = [Category::class], version =1, exportSchema = false )
abstract class CategoryDatabase: RoomDatabase() {

    abstract fun categoryDao():CategoryDao

    companion object{
        @Volatile
        private lateinit var INSTANCE: CategoryDatabase

        fun getDataBase(context: Context): CategoryDatabase{
            synchronized(CategoryDatabase::class.java){
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        CategoryDatabase::class.java, "category_database"
                    ).build()
                }
            return  INSTANCE
        }
    }
}