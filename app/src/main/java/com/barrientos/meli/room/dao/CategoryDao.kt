package com.barrientos.meli.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.barrientos.meli.models.Category

@Dao
interface CategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCategory(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setCategories(categories: Array<Category>)

    @Query("SELECT * from categories_table ORDER BY name ASC")
    fun getCategories() : LiveData<Array<Category>>

    @Query("DELETE FROM categories_table")
    fun deleteAll()
}