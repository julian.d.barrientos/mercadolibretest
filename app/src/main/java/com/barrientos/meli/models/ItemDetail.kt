package com.barrientos.meli.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class ItemDetail(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("site_id")
    var siteId:String,
    @Expose
    @SerializedName("title")
    var title: String,
    @Expose
    @SerializedName("price")
    var price: Number,
    @Expose
    @SerializedName("pictures")
    var pictures: List<Pictures>,
    @Expose
    @SerializedName("condition")
    var condition: String,
    @Expose
    @SerializedName("sold_quantity")
    var soldQuantity: String,
    @Expose
    @SerializedName("attributes")
    var attributes: List<Attribute>,
    @Expose
    @SerializedName("seller_id")
    var sellerId: String,
    @Expose
    @SerializedName("catalog_product_id")
    var catalogProductId: String?,
    @Expose
    @SerializedName("available_quantity")
    var availableQuantity: Number
)

data class Attribute(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("value_id")
    var valueId: String,
    @Expose
    @SerializedName("value_name")
    var valueName: String,
    @Expose
    @SerializedName("value_struct")
    var valueStruct: Struct,
    @Expose
    @SerializedName("values")
    var values: List<Value>,
    @Expose
    @SerializedName("attribute_group_id")
    var attributeGroupId: String,
    @Expose
    @SerializedName("attribute_group_name")
    var attributeGroupNname: String

)

data class Value(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("struct")
    var struct: Struct
)

data class Struct(
    @Expose
    @SerializedName("number")
    var number: Number,
    @Expose
    @SerializedName("unit")
    var unit: String
)

data class Pictures (
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("url")
    var url: String,
    @Expose
    @SerializedName("secure_url")
    var secureUrl: String,
    @Expose
    @SerializedName("size")
    var size: String,
    @Expose
    @SerializedName("quality")
    var quality: String
)