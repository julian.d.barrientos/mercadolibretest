package com.barrientos.meli.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Response (

    @Expose
    @SerializedName("site_id")
    var siteId : String,
    @Expose
    @SerializedName("paging")
    var paging : Paging,
    @Expose
    @SerializedName("results")
    var results : List<Item>,
    @Expose
    @SerializedName("secondary_results")
    var secondaryResults : List<Item>,
    @Expose
    @SerializedName("related_results")
    var relatedResults : List<Item> ,
    @Expose
    @SerializedName("sort")
    var sort: MapClass,
    @Expose
    @SerializedName("available_sorts")
    var availableSorts: List<MapClass>,
    @Expose
    @SerializedName("filters")
    var filters: List<Filter>,
    @Expose
    @SerializedName("available_filters")
    var availableFilters: List<AvailableFilter>
)

data class Paging (
    @Expose
    @SerializedName("total")
    var total : Number,
    @Expose
    @SerializedName("offset")
    var offset : Number,
    @Expose
    @SerializedName("limit")
    var limit : Number,
    @Expose
    @SerializedName("primary_results")
    var primaryResults : Number
)

data class Filter (
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("type")
    var type: String,
    @Expose
    @SerializedName("values")
    var values: List<ValueFilters>
)

data class ValueFilters(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("path_from_root")
    var pathFromRoot: List<MapClass>
)

data class AvailableFilter (
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("type")
    var type: String,
    @Expose
    @SerializedName("values")
    var values: List<ValueAvailableFilter>
)

data class ValueAvailableFilter (
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("name")
    var name: String,
    @Expose
    @SerializedName("results")
    var results: Long
)


