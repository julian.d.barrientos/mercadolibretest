package com.barrientos.meli.models

data class Catalog (
    val id: String,
    val domainID: String,
    val name: String,
    val parentID: String,
    val childrenIDS: List<Any?>,
    val attributes: List<Attribute>,
    val images: List<Image>,
    val lastUpdated: String
)

data class Image (
    val id: String,
    val url: String,
    val secureURL: String
)


