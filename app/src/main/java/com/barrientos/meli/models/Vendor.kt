package com.barrientos.meli.models

data class Vendor (
    val id: Long,
    val nickname: String,
    val registrationDate: String,
    val countryID: String,
    val address: Address,
    val userType: String,
    val tags: List<String>,
    val logo: String? = null,
    val points: Long,
    val siteID: String,
    val permalink: String,
    val sellerReputation: SellerReputation,
    val buyerReputation: BuyerReputation,
    val status: Status
)

data class Address (
    val city: String,
    val state: String
)

data class BuyerReputation (
    val tags: List<Any?>
)

data class SellerReputation (
    val levelID: String,
    val powerSellerStatus: Any? = null,
    val transactions: Transactions
)

data class Transactions (
    val canceled: Long,
    val completed: Long,
    val period: String,
    val ratings: Ratings,
    val total: Long
)

data class Ratings (
    val negative: Double,
    val neutral: Double,
    val positive: Double
)

data class Status (
    val siteStatus: String
)