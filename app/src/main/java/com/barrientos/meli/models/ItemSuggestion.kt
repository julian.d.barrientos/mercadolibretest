package com.barrientos.meli.models

import android.os.Parcel
import android.os.Parcelable
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion

class ItemSuggestion(_itemName:String="") : SearchSuggestion{

    var mItemName: String = _itemName

    constructor(parcel: Parcel) : this() {
        this.mItemName = parcel.readString().toString()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(mItemName)
    }

    override fun describeContents(): Int {
       return 0
    }

    override fun getBody(): String {
        return mItemName
    }

    companion object CREATOR : Parcelable.Creator<ItemSuggestion> {
        override fun createFromParcel(parcel: Parcel): ItemSuggestion {
            return ItemSuggestion(parcel)
        }

        override fun newArray(size: Int): Array<ItemSuggestion?> {
            return arrayOfNulls(size)
        }
    }

}