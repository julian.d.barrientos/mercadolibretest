package com.barrientos.meli.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "categories_table")
data class Category(
    @PrimaryKey
    @ColumnInfo(name = "id")
    @Expose
    @SerializedName("id")
    var id:String,

    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name:String
):Serializable

