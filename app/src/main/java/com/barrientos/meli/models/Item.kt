package com.barrientos.meli.models

import java.io.Serializable

class Item () : Serializable{
    var id: String = ""
    var siteID: String = ""
    var title: String = ""
    var seller: Seller = Seller()
    var price: Double = 0.0
    var currencyID: String = ""
    var availableQuantity: Long = 0
    var sold_quantity: Long = 0
    var buyingMode: String = ""
    var listingTypeID: String = ""
    var stopTime: String = ""
    var condition: String = ""
    var permalink: String = ""
    var thumbnail: String = ""
    var acceptsMercadopago: Boolean = false
    var installments: Installments = Installments()
    var address: Address = Address()
    var shipping: Shipping = Shipping()
    var sellerAddress: SellerAddress = SellerAddress()
    var attributes: List<Attribute> = listOf()
    var differentialPricing: DifferentialPricing =  DifferentialPricing()
    var originalPrice: Long = 0
    var categoryID: String = ""
    var officialStoreID: Number = 0
    var catalogProductID: String = ""
    var tags: List<String> = listOf()
    var catalogListing: Boolean = false

    constructor(id: String, siteID: String, title: String, seller: Seller, price: Double, currencyID: String, availableQuantity: Long, soldQuantity:Long, buyingMode: String, listingTypeID: String,
                stopTime: String, condition: String, permalink: String, thumbnail: String, acceptsMercadopago: Boolean, installments: Installments, address: Address, shipping: Shipping,
                sellerAddress: SellerAddress, attributes: List<Attribute>, differentialPricing: DifferentialPricing, originalPrice: Long, categoryID: String, officialStoreID: Number,
                catalogProductID: String, tags: List<String>, catalogListing: Boolean):this() {
        this.id = id
        this.siteID = siteID
        this.seller = seller
        this.title = title
        this.price = price
        this.currencyID = currencyID
        this.availableQuantity = availableQuantity
        this.sold_quantity = soldQuantity
        this.buyingMode = buyingMode
        this.listingTypeID = listingTypeID
        this.stopTime = stopTime
        this.condition = condition
        this.permalink = permalink
        this.thumbnail = thumbnail
        this.acceptsMercadopago = acceptsMercadopago
        this.installments = installments
        this.address = address
        this.shipping = shipping
        this.sellerAddress = sellerAddress
        this.attributes = attributes
        this.differentialPricing = differentialPricing
        this.originalPrice = originalPrice
        this.categoryID = categoryID
        this.officialStoreID = officialStoreID
        this.catalogProductID = catalogProductID
        this.tags = tags
        this.catalogListing = catalogListing
    }

    inner class Address(): Serializable {
        var stateID: String = ""
        var stateName: String = ""
        var cityID: String = ""
        var cityName: String = ""
        constructor(stateID: String, stateName: String, cityID: String, cityName: String) : this() {
            this.stateID = stateID
            this.stateName = stateName
            this.cityID = cityID
            this.cityName = cityName
        }
    }

    inner class Attribute(): Serializable {
        var values: List<Value> = listOf()
        var attributeGroupID: String = ""
        var attributeGroupName: String = ""
        var id: String = ""
        var valueID: String = ""
        var valueName: String = ""
        var valueStruct: Any? = null
        var source: Long = 0
        var name: String = ""
        constructor(values: List<Value> , attributeGroupID: String, attributeGroupName: String, id: String, valueID: String, valueName: String, valueStruct: Any?, source: Long, name: String):this(){
            this.values = values
            this.attributeGroupID = attributeGroupID
            this.attributeGroupName = attributeGroupName
            this.id = id
            this.valueID = valueID
            this.valueName = valueName
            this.valueStruct = valueStruct
            this.source = source
            this.name = name
        }
    }

    inner class Value (): Serializable {
        var source: Long = 0
        var id: String = ""
        var name: String = ""
        var struct: Any? = null

        constructor(source: Long, id: String, name: String, struct: Any?):this(){
            this.source = source
            this.id = id
            this.name =name
            this.struct = struct
        }
    }

    inner class DifferentialPricing () : Serializable {
        var id: Long = 0
        constructor(id: Long):this(){
            this.id = id
        }
    }

    inner class Installments (): Serializable {
        var quantity: Long = 0
        var amount: Double = 0.0
        var rate: Double = 0.0
        var currencyID: String = ""
        constructor(quantity: Long, amount: Double, rate: Double, currencyID: String):this() {
            this.quantity = quantity
            this.amount = amount
            this.rate = rate
            this.currencyID = currencyID
        }
    }

    inner class Seller (): Serializable {
        var id: Long = 0
        var powerSellerStatus: Any? = null
        var carDealer: Boolean = false
        var realEstateAgency: Boolean = false
        var tags: List<Any?> = listOf()
        constructor(id: Long, powerSellerStatus: Any?, carDealer: Boolean, realEstateAgency: Boolean, tags: List<Any?>):this() {
            this.id = id
            this.powerSellerStatus = powerSellerStatus
            this.carDealer = carDealer
            this.realEstateAgency = realEstateAgency
            this.tags = tags
        }
    }

    inner class SellerAddress (): Serializable {
        var id: String = ""
        var comment: String = ""
        var addressLine: String = ""
        var zipCode: String = ""
        var country: MapClass = MapClass()
        var state: MapClass = MapClass()
        var city: MapClass = MapClass()
        var latitude: String = ""
        var longitude: String = ""

        constructor(id: String, comment: String, addressLine: String, zipCode: String, country: MapClass, state: MapClass, city: MapClass, latitude: String, longitude: String):this() {
            this.id = id
            this.comment = comment
            this.addressLine = addressLine
            this.zipCode = zipCode
            this.country = country
            this.state = state
            this.city = city
            this.latitude = latitude
            this.longitude = longitude
        }
    }



    inner class Shipping (): Serializable {
        var freeShipping: Boolean = false
        var mode: String = ""
        var tags: List<String> = listOf()
        var logisticType: String = ""
        var storePickUp: Boolean = false

        constructor(freeShipping: Boolean, mode: String, tags: List<String>, logisticType: String, storePickUp: Boolean):this() {
            this.freeShipping = freeShipping
            this.mode = mode
            this.tags = tags
            this.logisticType = logisticType
            this.storePickUp = storePickUp
        }
    }
}



